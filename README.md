## 开发
```bash
#需要修改的地方
request.js 里面的baseURL改为你的后端地址
vue.config.js 里面的target改为你的后端地址

# 克隆项目
git clone https://gitee.com/y_project/zhglpt-Vue

# 进入项目目录
cd zhglpt-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```
## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 生产环境部署方式一
```bash
==========================nginx下部署===========================
# 前后端分离式部署VUE_APP_BASE_API = '/'即可  前后端集成部署VUE_APP_BASE_API = '//localhost:9100'
1、运行代码：  
npm run build:prod
生成一个dist文件夹

2、修改nginx下conf下的nginx.conf文件
  location / {
    root   E:/myworkspace/ideaworkspace/zhglpt-ui/dist/;
    try_files $uri $uri/ /index.html;
    index  index.html index.htm;
  }
3、启动nginx，访问http://localhost/即可

==========================tomcat下部署===========================
1、运行代码：  
npm run build:prod
生成一个dist文件夹

2、复制dist里面的内容到tomcat webapps下的ROOT目录里面
3、启动tomcat，访问http://localhost:端口即可

==========================和后端项目一起部署===========================
1、运行代码：  
npm run build:prod
生成一个dist文件夹

2、复制dist里面的内容到后端项目的static目录下
3、启动后端项目，访问http://localhost:端口即可
```

## 生产环境部署方式二
```bash
1、重新编译项目
  npm run build:prod
2、maven运行package、install把前端项目打成jar包 放入maven库
3、后端引入
<!-- 本项目对应前端 -->
<dependency>
  <groupId>com.zhglpt</groupId>
  <artifactId>zhglpt-ui</artifactId>
  <version>0.0.1</version>
</dependency>
4、启动后端项目即可
```
## 笔记
```bash
# JSON.stringify打印object对象
console.log(JSON.stringify(Object))

# 浏览器访问 http://localhost/
用户名：admin
密码：111111
```
## 目录说明
```
api      访问后端的api接口
assets   静态资源存放目录
components 第三方组件存放目录
directive 权限目录
layout    页面布局目录
router    路由器
store     全局缓存目录
utils     工具包
views     前端页面

```