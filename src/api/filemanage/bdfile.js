import request from '@/utils/request'

// 查询参数列表
export function listBdfile(query) {
  return request({
    url: '/filemanage/bdfile/list',
    method: 'get',
    params: query
  })
}

// 进入文件夹
export function intoFile(data) {
  return request({
    url: '/filemanage/bdfile',
    method: 'post',
    data: data
  })
}

// 新建文件夹
export function newPackage(data) {
  return request({
    url: '/filemanage/bdfile/newPackage',
    method: 'post',
    data: data
  })
}

// 删除文件夹
export function delFile(data) {
  return request({
    url: '/filemanage/bdfile/delDir',
    method: 'post',
    data: data
  })
}

// 文件下载
export function fileDownLoad(data) {
  return request({
    url: '/filemanage/bdfile/download',
    method: 'post',
    data: data
  })
}

// 文件压缩
export function zippackage(data) {
  return request({
    url: '/filemanage/bdfile/package',
    method: 'post',
    data: data
  })
}

// 解压文件
export function unPackage(data) {
  return request({
    url: '/filemanage/bdfile/unPackage',
    method: 'post',
    data: data
  })
}

// 查询参数详细
export function getConfig(configId) {
  return request({
    url: '/system/config/' + configId,
    method: 'get'
  })
}

// 根据参数键名查询参数值
export function getConfigKey(configKey) {
  return request({
    url: '/system/config/configKey/' + configKey,
    method: 'get'
  })
}

// 新增参数配置
export function addConfig(data) {
  return request({
    url: '/system/config',
    method: 'post',
    data: data
  })
}

// 修改参数配置
export function updateConfig(data) {
  return request({
    url: '/system/config',
    method: 'put',
    data: data
  })
}

// 删除参数配置
export function delConfig(configId) {
  return request({
    url: '/system/config/' + configId,
    method: 'delete'
  })
}

// 刷新参数缓存
export function refreshCache() {
  return request({
    url: '/system/config/refreshCache',
    method: 'delete'
  })
}

// 导出参数
export function exportConfig(query) {
  return request({
    url: '/system/config/export',
    method: 'get',
    params: query
  })
}
